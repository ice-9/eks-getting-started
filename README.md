# Test task

This repo provide a code and CI to run k8s in EKS from scratch and deploy httpbin with helm.

To check CI result you need to get kubectl config from output of job `apply` and configure your local `kubectl`.

Than check output of job `deploy` and get tips to check `httpbin`.
